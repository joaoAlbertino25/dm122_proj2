import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Delivery } from "../../models/Delivery";
import { DeliveryService } from '../../services/Delivery.service';
import { Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the DetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-detalhes',
  templateUrl: 'detalhes.html',
})
export class DetalhesPage {

  //INSTANCE OF DELIVERY
  delivery:Delivery;
  constructor( public navCtrl:NavController , params: NavParams , private deliveryService:DeliveryService , public toast:ToastController) {
  
    this.delivery = new Delivery();
    this.delivery._id = params.data._id;
    this.delivery._detalhes = params.data._detalhes;
    this.delivery._produtos = params.data._produtos;
    this.delivery._status = params.data._status;
  }

  //UPDATE DATA
  updateData() {
    try{
      this.deliveryService.updateDelivery(this.delivery);   
      let toast = this.toast.create({
        message: 'atualizado com sucesso',
        duration: 3000,
        position: 'top'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      toast.present();
    }catch(e){

    }
  }

  //RETURN TO PREVIOUS PAGE
  backDeliveryPage(){
    this.navCtrl.pop();
  }


}
