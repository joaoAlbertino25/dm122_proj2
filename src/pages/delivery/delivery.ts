import { DeliveryService } from "../../services/Delivery.service";
import { Observable } from 'rxjs/Observable';
import { Component  } from '@angular/core';
import { AngularFirestoreCollection } from "angularfire2/firestore";
import { map } from 'rxjs/operators';
import { Delivery } from "../../models/Delivery";
import { Status } from "../../enums/Status.enum";
import { Product } from "../../models/Product";
import { NavController, ModalController } from 'ionic-angular';
import { DetalhesPage } from '../detalhes/detalhes';
import { Events } from 'ionic-angular';

/**
 * Generated class for the DeliveryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.html',
})
export class DeliveryPage {
  
  itemsRef: AngularFirestoreCollection<any>;
  listDeliverys: Observable<any[]>;
  resultDeliverys: Array<any>;
  delivery:Delivery;
  product:Product;
  newDetalhes:DetalhesPage;
  searchDel:string = '';
  constructor(private deliveryService:DeliveryService , private modalCtrl:ModalController , events:Events) {
    events.subscribe("delivery:list",()=>{
      this.loadList();
    });
    this.loadList();
  }

  bootstrap (){
    let detalhes = ["teste" , "teste1" , "teste2"];

    for (let i = 0 ; i < 3 ; i ++){
      this.delivery = new Delivery();
      this.product = new Product();

      this.product._description = "carro";
      this.product._name = "KA";
      this.product._price = 12;

      this.delivery._detalhes = "teste";
      this.delivery._status = Status.EM_TRAJETO;
      this.delivery._produtos = this.product ;

      this.deliveryService.addDelivery(this.delivery);
    }
   
  }

  openDetalhes(delivery){
    this.delivery = delivery as Delivery;
    let detalhesModal = this.modalCtrl.create(DetalhesPage, this.delivery);
    detalhesModal.present();
  }

  loadList(filter?:string){
    console.log("teste");
    this.resultDeliverys = new Array<any>();
    this.listDeliverys = new Observable<any>();
    this.listDeliverys = this.deliveryService.getAllDelivery();
    this.listDeliverys.subscribe((changes) => {
          this.resultDeliverys = new Array<any>();
          console.log(changes);
          changes.map( sub =>{
            if(filter){
              if(sub._status.toLowerCase() === filter.toLowerCase() || sub._produtos._name.toLowerCase() === filter.toLowerCase()){
                this.resultDeliverys.push(sub);
              }
            }else{
              this.resultDeliverys.push(sub);
            }
            
          })
    },
    (err) =>{
          console.log(err);
    });
  }

  searchList(){
   this.loadList(this.searchDel);
  }

  
}
