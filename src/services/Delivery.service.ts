import {Injectable} from "@angular/core";
import { Delivery } from  "../models/Delivery";
import { GeneralConsts } from "../conts/generalConsts";
import { AngularFireDatabase  } from "angularfire2/database";
import { Observable } from "rxjs/Observable";
import { AngularFireList, AngularFireObject } from "angularfire2/database/interfaces";
import { map } from "rxjs/operators/map";

//DELIVERY SERVICE 

//responsible for CRUD operations in Delivery objects
@Injectable()
export class DeliveryService {

    //LIST OF REFERENCE OBJECTS FROM DATABASE
    listDelivery: AngularFireList<any>;

    //LIST OF OBSERVABLES OBJECTS FROM DETABASE
    deliverys: Observable<any[]>;

    //CONSTRUCTOR 
    constructor(private angularDatabase:AngularFireDatabase){
        this.loadData();        
    }
    
    //CRUD OPERATIONS

    //ADD
    addDelivery(newDelivery:Delivery){
        this.listDelivery.push(newDelivery);
        
    }

    //UPDATE 
    updateDelivery(newDelivery:Delivery){
        let list = this.listDelivery;
        let ref = this.angularDatabase.database.ref(GeneralConsts.STORAGE_DELIVERY);
        let query = ref.orderByKey().on("value", function (obj){
            obj.forEach(function(value){ 
                if(value.val()._id == newDelivery._id){
                    list.update(value.key  ,newDelivery);
                }
            }); 
        }); 
    }

    //GET
    getAllDelivery(){
       return this.deliverys;
    }


    //INTERNAL FUNCTION TOM LOAD DATA 
    private loadData(){
        this.listDelivery = this.angularDatabase.list(GeneralConsts.STORAGE_DELIVERY);
        console.log(this.listDelivery);
        this.deliverys = this.listDelivery.valueChanges();
    }

}