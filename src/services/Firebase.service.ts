import firebase from "firebase";
import { Injectable } from "@angular/core";

@Injectable()
export class FirebaseService {

    DATA_URL = firebase.storage.StringFormat.DATA_URL;

    getReferenceFromChild(resource) {
        let storageREF = firebase.storage().ref();
        let resourceREF = storageREF.child(resource);
        return resourceREF;
    }

}