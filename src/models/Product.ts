
export class Product {

    _name:string;
    _description:string;
    _price:number;

    constructor(name?:string , description?:string , price?:number){
        this._name = name;
        this._description = description;
        this._price = price;
    }

    set name (name:string){
        this._name = name;
    }

    set description (description:string){
        this._description = description;
    }

    set price (price:number){
         this._price = price;
    }

    get name (){
        return this._name;   
    }

    get description (){
        return this._description;
    }

    get price (){
        return this._price;
    }


}