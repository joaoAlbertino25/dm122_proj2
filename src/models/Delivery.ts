import {Status} from "../enums/Status.enum";
import {Product} from "./Product";

export class Delivery {

    _id:string;
    _status:Status;
    _produtos:Product;
    _detalhes:String;

    constructor(status?:string , produtos?:Product , detalhes?:string){
        this._id = this.generateId();
        this._status = status !== "" || status !== null ? Status[status] : null;
        this._produtos = produtos;
        this._detalhes = detalhes;
    }

    set status (status:string){
        this._status = Status[status];
    } 

    set produtos (produtos:Product){
        this._produtos = produtos;
    }

    set detalhes (detalhes:String){
        this._detalhes = detalhes;
    }

    get status (){
        return this._status;
    }

    get produtos (){
        return this._produtos;
    }

    get detalhes (){
        return this._detalhes;
    }

    private generateId(){
        let date = new Date().getDate();
        let ramdom = Math.floor((Math.random() * 10000) + 1);
        return date+"_"+ramdom;
    }

}