import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DeliveryPage } from "../pages/delivery/delivery";
import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = DeliveryPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();


      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAYhtI9TEdc99mBGO6MMRxqdyGLTLTZTmg",
        authDomain: "myproject-97b5a.firebaseapp.com",
        databaseURL: "https://myproject-97b5a.firebaseio.com",
        projectId: "myproject-97b5a",
        storageBucket: "myproject-97b5a.appspot.com",
        messagingSenderId: "301392410968"
      };
      //firebase.initializeApp(config);

    });
  }
}
