import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { DetalhesPage } from '../pages/detalhes/detalhes';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DeliveryService } from "../services/Delivery.service";
import { DeliveryPage } from "../pages/delivery/delivery";
import { FirebaseService } from "../services/Firebase.service";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireModule   } from 'angularfire2';
import { Enviroment } from "../enviroments/Enviroment";
import { AngularFirestore } from "angularfire2/firestore";
import firebase from "firebase";


@NgModule({
  declarations: [
    MyApp,
    DeliveryPage,
    DetalhesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(Enviroment.config),
    AngularFirestoreModule,
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DeliveryPage,
    DetalhesPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DeliveryService,
    AngularFireDatabase,
    {provide: FirebaseService , useClass: FirebaseService},
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
